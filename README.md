# Payday 2 Chat Clock

A mod for Payday 2 that adds timestamps to chat messages

## How to install

1. Get [Payday 2 BLT](https://paydaymods.com/download/) and follow the install instructions provided
2. Create a new folder in PAYDAY 2/mods
3. Place the contents of this mod in the new folder you made

## Features

- Adds a timestamp before every message in chat
- Chose between in game time or real time
- Contains multiple clock formats